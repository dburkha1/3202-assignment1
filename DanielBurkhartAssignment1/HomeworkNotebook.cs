﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DanielBurkhartAssignment1.Properties;
using HomeworkNotebookControl;

namespace DanielBurkhartAssignment1
{
    /// <summary>
    ///     The homework notebook form class.
    /// </summary>
    public partial class HomeworkNotebook : Form
    {
        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="HomeworkNotebook" /> class.
        /// </summary>
        public HomeworkNotebook()
        {
            this.InitializeComponent();

            this.homeworkNotebookTabControl.DrawItem += this.homeworkNotebookTabControl_DrawItem;

            this.addEventHandlerToEachControl();

            this.addInitialClassesToControls();

            this.tabColors.Add(this.tabPage1, Color.White);
            this.tabColors.Add(this.tabPage2, Color.White);
            this.tabColors.Add(this.tabPage3, Color.White);
        }

        #endregion

        #region Instance Variables

        private string textBoxString;

        private readonly Dictionary<TabPage, Color> tabColors = new Dictionary<TabPage, Color>();

        #endregion

        #region Methods

        private void addEventHandlerToEachControl()
        {
            this.homeworkNotebookUserControl1.ControlModified += this.buildOutput;
            this.homeworkNotebookUserControl2.ControlModified += this.buildOutput;
            this.homeworkNotebookUserControl3.ControlModified += this.buildOutput;
        }

        private void addInitialClassesToControls()
        {
            this.homeworkNotebookUserControl1.AddTask("Go to Class.");
            this.homeworkNotebookUserControl1.AddTask("Read up on C++");
            this.homeworkNotebookUserControl2.AddTask("Learn more on JQuery");
            this.homeworkNotebookUserControl2.AddTask("See if library has any useful resources on Javascript");
            this.homeworkNotebookUserControl3.AddTask("Read chapter 5 of textbook");
        }

        #region Tab Headers Event

        private void homeworkNotebookTabControl_DrawItem(object sender, DrawItemEventArgs e)
        {
            using (Brush br = new SolidBrush(this.tabColors[this.homeworkNotebookTabControl.TabPages[e.Index]]))
            {
                e.Graphics.FillRectangle(br, e.Bounds);
                var sz = e.Graphics.MeasureString(this.homeworkNotebookTabControl.TabPages[e.Index].Text, e.Font);
                e.Graphics.DrawString(this.homeworkNotebookTabControl.TabPages[e.Index].Text, e.Font, Brushes.Black,
                    e.Bounds.Left + (e.Bounds.Width - sz.Width)/2, e.Bounds.Top + (e.Bounds.Height - sz.Height)/2 + 1);

                var rect = e.Bounds;
                rect.Offset(0, 1);
                rect.Inflate(0, -1);
                e.Graphics.DrawRectangle(Pens.DarkGray, rect);
                e.DrawFocusRectangle();
            }
        }

        private void determineHeaderColor(HomeworkNotebookUserControl userControl)
        {
            var currentTabPage = TabPage.GetTabPageOfComponent(userControl);

            if (userControl == null)
            {
                return;
            }

            switch (userControl.Priority)
            {
                case HomeworkNotebookUserControl.PriorityLevel.High:

                    this.setTabHeader(currentTabPage, Color.Red);
                    break;

                case HomeworkNotebookUserControl.PriorityLevel.Medium:
                    this.setTabHeader(currentTabPage, Color.Yellow);

                    break;

                default:
                    this.setTabHeader(currentTabPage, Color.GhostWhite);
                    break;
            }
        }

        private void setTabHeader(TabPage page, Color color)
        {
            this.tabColors[page] = color;
            this.homeworkNotebookTabControl.Invalidate();
        }

        #endregion

        #region Output Methods

        private void buildOutput(object sender, UserControlEventArgs e)
        {
            this.summaryOutputTextBox.Clear();
            this.textBoxString = string.Empty;

            foreach (TabPage tabPage in this.homeworkNotebookTabControl.TabPages)
            {
                var currentUserControl =
                    tabPage.Controls.Cast<HomeworkNotebookUserControl>()
                           .FirstOrDefault(x => x != null);

                if (currentUserControl == null)
                {
                    continue;
                }

                this.makeOutputFromUserControl(currentUserControl, tabPage.Text);
                this.determineHeaderColor(currentUserControl);
            }

            this.summaryOutputTextBox.Text = this.textBoxString;
        }

        private void makeOutputFromUserControl(HomeworkNotebookUserControl control, string className)
        {
            if (control.FindAllUnfinishedTasks().Count < 1)
            {
                return;
            }

            this.textBoxString += Environment.NewLine + this.priorityLevelHeading(control.Priority) +
                                  Environment.NewLine;
            this.textBoxString += className + Resources.colon + Environment.NewLine;

            this.getUnfinishedTasks(control);
        }

        private void getUnfinishedTasks(HomeworkNotebookUserControl control)
        {
            var currentUserControlList = control.FindAllUnfinishedTasks();
            foreach (var currentUnfinishedTask in currentUserControlList)
            {
                this.textBoxString += currentUnfinishedTask + Environment.NewLine;
            }
        }

        private string priorityLevelHeading(HomeworkNotebookUserControl.PriorityLevel currentPriorityLevel)
        {
            var headerString = string.Empty;

            if (currentPriorityLevel.Equals(HomeworkNotebookUserControl.PriorityLevel.High))
            {
                headerString = "Priority Level - High" + Environment.NewLine + "--------------------------------";
            }
            else if (currentPriorityLevel.Equals(HomeworkNotebookUserControl.PriorityLevel.Medium))
            {
                headerString = "Priority Level - Medium" + Environment.NewLine + "--------------------------------";
            }
            else if (currentPriorityLevel.Equals(HomeworkNotebookUserControl.PriorityLevel.Low))
            {
                headerString = "Priority Level - Low" + Environment.NewLine + "--------------------------------";
            }

            return headerString;
        }

        #endregion

        #endregion
    }
}