﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace HomeworkNotebookControl
{
    /// <summary>
    ///     The User control for the notebook.
    /// </summary>
    public partial class HomeworkNotebookUserControl : UserControl
    {
        #region Enumeration

        /// <summary>
        ///     Enum of all the accepted types of the priority level.
        /// </summary>
        public enum PriorityLevel
        {
            High,
            Medium,
            Low,
            Unknown
        }

        #endregion

        #region Instance Variable

        private readonly List<string> unfinishedTasks = new List<string>();

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="HomeworkNotebookUserControl" /> class.
        /// </summary>
        public HomeworkNotebookUserControl()
        {
            this.InitializeComponent();
            this.lowRadioButton.Checked = true;
            this.lowRadioButton.Tag = PriorityLevel.Low;
            this.highRadioButton.Tag = PriorityLevel.High;
            this.mediumRadioButton.Tag = PriorityLevel.Medium;
        }

        #endregion

        #region Property

        /// <summary>
        ///     Gets or sets the priority level of the user control.
        /// </summary>
        /// <value>
        ///     The priority.
        /// </value>
        public PriorityLevel Priority
        {
            get
            {
                var checkedButton =
                    this.radioButtonGroupBox.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                if (checkedButton == null)
                {
                    return PriorityLevel.Unknown;
                }
                return (PriorityLevel) checkedButton.Tag;
            }

            set
            {
                switch (value)
                {
                    case PriorityLevel.High:
                        this.highRadioButton.Checked = true;

                        break;
                    case PriorityLevel.Medium:
                        this.mediumRadioButton.Checked = true;

                        break;
                    case PriorityLevel.Low:
                        this.lowRadioButton.Checked = true;

                        break;
                }
            }
        }

        #endregion

        #region Methods

        private void checkAllButton_Click(object sender, EventArgs e)
        {
            this.checkOrUncheckAllTasks(true);
            this.OnControlModified();
        }

        private void uncheckAllButton_Click(object sender, EventArgs e)
        {
            this.checkOrUncheckAllTasks(false);
            this.OnControlModified();
        }

        private void checkOrUncheckAllTasks(bool trueOrFalse)
        {
            foreach (DataGridViewRow currentRow in this.tasksDataGridView.Rows)
            {
                ((DataGridViewCheckBoxCell) currentRow.Cells[0]).Value = trueOrFalse;
            }
        }

        /// <summary>
        ///     Adds the task to the data grid view.
        /// </summary>
        /// <param name="taskToAdd">The task to add.</param>
        public void AddTask(string taskToAdd)
        {
            this.tasksDataGridView.Rows.Add(0, taskToAdd);
        }

        /// <summary>
        ///     Finds all unfinished tasks.
        /// </summary>
        /// <returns></returns>
        public List<string> FindAllUnfinishedTasks()
        {
            this.unfinishedTasks.Clear();
            foreach (DataGridViewRow currentRowOfGrid in this.tasksDataGridView.Rows)
            {
                var cell = (DataGridViewCheckBoxCell) currentRowOfGrid.Cells[0];
                this.checkEachCell(cell, currentRowOfGrid);
            }

            return this.unfinishedTasks;
        }

        private void checkEachCell(DataGridViewCell cell, DataGridViewRow currentRowOfGrid)
        {
            if (Convert.ToBoolean(cell.EditedFormattedValue))
            {
                return;
            }
            var textCell = (DataGridViewTextBoxCell) currentRowOfGrid.Cells[1];
            this.addUnfishedTaskToList(textCell);
        }

        private void addUnfishedTaskToList(DataGridViewTextBoxCell textCell)
        {
            if (textCell.Value != null)
            {
                this.unfinishedTasks.Add(textCell.Value.ToString());
            }
        }

        #endregion

        #region Event declaration and Method

        /// <summary>
        ///     Occurs when [control modified].
        /// </summary>
        public event EventHandler<UserControlEventArgs> ControlModified;

        /// <summary>
        ///     Called when [control modified].
        /// </summary>
        public void OnControlModified()
        {
            var handler = this.ControlModified;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        #endregion

        #region Event-driven Methods

        private void tasksDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.OnControlModified();
        }

        private void tasksDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.OnControlModified();
        }

        private void highRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnControlModified();
        }

        private void mediumRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnControlModified();
        }

        private void lowRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnControlModified();
        }

        private void HomeworkNotebookUserControl_Load(object sender, EventArgs e)
        {
            this.OnControlModified();
        }

        #endregion
    }

    #region Event Class

    public class UserControlEventArgs : EventArgs
    {
    }

    #endregion
}