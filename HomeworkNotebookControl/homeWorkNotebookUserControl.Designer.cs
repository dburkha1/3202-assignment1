﻿namespace HomeworkNotebookControl
{
    partial class HomeworkNotebookUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonGroupBox = new System.Windows.Forms.GroupBox();
            this.lowRadioButton = new System.Windows.Forms.RadioButton();
            this.mediumRadioButton = new System.Windows.Forms.RadioButton();
            this.highRadioButton = new System.Windows.Forms.RadioButton();
            this.tasksDataGridView = new System.Windows.Forms.DataGridView();
            this.doneCheckColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.taskTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkAllButton = new System.Windows.Forms.Button();
            this.uncheckAllButton = new System.Windows.Forms.Button();
            this.radioButtonGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tasksDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButtonGroupBox
            // 
            this.radioButtonGroupBox.Controls.Add(this.lowRadioButton);
            this.radioButtonGroupBox.Controls.Add(this.mediumRadioButton);
            this.radioButtonGroupBox.Controls.Add(this.highRadioButton);
            this.radioButtonGroupBox.Location = new System.Drawing.Point(18, 21);
            this.radioButtonGroupBox.Name = "radioButtonGroupBox";
            this.radioButtonGroupBox.Size = new System.Drawing.Size(204, 222);
            this.radioButtonGroupBox.TabIndex = 0;
            this.radioButtonGroupBox.TabStop = false;
            this.radioButtonGroupBox.Text = "Priority";
            // 
            // lowRadioButton
            // 
            this.lowRadioButton.AutoSize = true;
            this.lowRadioButton.Checked = true;
            this.lowRadioButton.Location = new System.Drawing.Point(7, 124);
            this.lowRadioButton.Name = "lowRadioButton";
            this.lowRadioButton.Size = new System.Drawing.Size(82, 29);
            this.lowRadioButton.TabIndex = 2;
            this.lowRadioButton.TabStop = true;
            this.lowRadioButton.Text = "Low";
            this.lowRadioButton.UseVisualStyleBackColor = true;
            this.lowRadioButton.CheckedChanged += new System.EventHandler(this.lowRadioButton_CheckedChanged);
            // 
            // mediumRadioButton
            // 
            this.mediumRadioButton.AutoSize = true;
            this.mediumRadioButton.Location = new System.Drawing.Point(7, 89);
            this.mediumRadioButton.Name = "mediumRadioButton";
            this.mediumRadioButton.Size = new System.Drawing.Size(119, 29);
            this.mediumRadioButton.TabIndex = 1;
            this.mediumRadioButton.Text = "Medium";
            this.mediumRadioButton.UseVisualStyleBackColor = true;
            this.mediumRadioButton.CheckedChanged += new System.EventHandler(this.mediumRadioButton_CheckedChanged);
            // 
            // highRadioButton
            // 
            this.highRadioButton.AutoSize = true;
            this.highRadioButton.Location = new System.Drawing.Point(7, 54);
            this.highRadioButton.Name = "highRadioButton";
            this.highRadioButton.Size = new System.Drawing.Size(87, 29);
            this.highRadioButton.TabIndex = 0;
            this.highRadioButton.Text = "High";
            this.highRadioButton.UseVisualStyleBackColor = true;
            this.highRadioButton.CheckedChanged += new System.EventHandler(this.highRadioButton_CheckedChanged);
            // 
            // tasksDataGridView
            // 
            this.tasksDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.tasksDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tasksDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.doneCheckColumn,
            this.taskTextBoxColumn});
            this.tasksDataGridView.Location = new System.Drawing.Point(228, 21);
            this.tasksDataGridView.Name = "tasksDataGridView";
            this.tasksDataGridView.RowHeadersVisible = false;
            this.tasksDataGridView.Size = new System.Drawing.Size(1192, 464);
            this.tasksDataGridView.TabIndex = 1;
            this.tasksDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tasksDataGridView_CellContentClick);
            this.tasksDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.tasksDataGridView_CellEndEdit);
            // 
            // doneCheckColumn
            // 
            this.doneCheckColumn.HeaderText = "Done";
            this.doneCheckColumn.Name = "doneCheckColumn";
            this.doneCheckColumn.Width = 69;
            // 
            // taskTextBoxColumn
            // 
            this.taskTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.taskTextBoxColumn.HeaderText = "Task";
            this.taskTextBoxColumn.Name = "taskTextBoxColumn";
            // 
            // checkAllButton
            // 
            this.checkAllButton.Location = new System.Drawing.Point(624, 509);
            this.checkAllButton.Name = "checkAllButton";
            this.checkAllButton.Size = new System.Drawing.Size(133, 45);
            this.checkAllButton.TabIndex = 2;
            this.checkAllButton.Text = "Check All";
            this.checkAllButton.UseVisualStyleBackColor = true;
            this.checkAllButton.Click += new System.EventHandler(this.checkAllButton_Click);
            // 
            // uncheckAllButton
            // 
            this.uncheckAllButton.Location = new System.Drawing.Point(902, 509);
            this.uncheckAllButton.Name = "uncheckAllButton";
            this.uncheckAllButton.Size = new System.Drawing.Size(137, 45);
            this.uncheckAllButton.TabIndex = 3;
            this.uncheckAllButton.Text = "Uncheck All";
            this.uncheckAllButton.UseVisualStyleBackColor = true;
            this.uncheckAllButton.Click += new System.EventHandler(this.uncheckAllButton_Click);
            // 
            // HomeworkNotebookUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.uncheckAllButton);
            this.Controls.Add(this.checkAllButton);
            this.Controls.Add(this.tasksDataGridView);
            this.Controls.Add(this.radioButtonGroupBox);
            this.Name = "HomeworkNotebookUserControl";
            this.Size = new System.Drawing.Size(1444, 594);
            this.Load += new System.EventHandler(this.HomeworkNotebookUserControl_Load);
            this.radioButtonGroupBox.ResumeLayout(false);
            this.radioButtonGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tasksDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox radioButtonGroupBox;
        private System.Windows.Forms.RadioButton lowRadioButton;
        private System.Windows.Forms.RadioButton mediumRadioButton;
        private System.Windows.Forms.RadioButton highRadioButton;
        private System.Windows.Forms.DataGridView tasksDataGridView;
        private System.Windows.Forms.Button checkAllButton;
        private System.Windows.Forms.Button uncheckAllButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn doneCheckColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskTextBoxColumn;
    }
}
