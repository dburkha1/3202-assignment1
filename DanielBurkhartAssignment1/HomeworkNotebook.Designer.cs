﻿namespace DanielBurkhartAssignment1
{
    partial class HomeworkNotebook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.homeworkNotebookTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.homeworkNotebookUserControl1 = new HomeworkNotebookControl.HomeworkNotebookUserControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.homeworkNotebookUserControl2 = new HomeworkNotebookControl.HomeworkNotebookUserControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.homeworkNotebookUserControl3 = new HomeworkNotebookControl.HomeworkNotebookUserControl();
            this.coursesLabel = new System.Windows.Forms.Label();
            this.tasksToCompleteLabel = new System.Windows.Forms.Label();
            this.summaryOutputTextBox = new System.Windows.Forms.TextBox();
            this.homeworkNotebookTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // homeworkNotebookTabControl
            // 
            this.homeworkNotebookTabControl.Controls.Add(this.tabPage1);
            this.homeworkNotebookTabControl.Controls.Add(this.tabPage2);
            this.homeworkNotebookTabControl.Controls.Add(this.tabPage3);
            this.homeworkNotebookTabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.homeworkNotebookTabControl.Location = new System.Drawing.Point(21, 69);
            this.homeworkNotebookTabControl.Name = "homeworkNotebookTabControl";
            this.homeworkNotebookTabControl.SelectedIndex = 0;
            this.homeworkNotebookTabControl.Size = new System.Drawing.Size(1462, 638);
            this.homeworkNotebookTabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.homeworkNotebookUserControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1454, 600);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CS 3202";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // homeworkNotebookUserControl1
            // 
            this.homeworkNotebookUserControl1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.homeworkNotebookUserControl1.Location = new System.Drawing.Point(0, 3);
            this.homeworkNotebookUserControl1.Name = "homeworkNotebookUserControl1";
            this.homeworkNotebookUserControl1.Priority = HomeworkNotebookControl.HomeworkNotebookUserControl.PriorityLevel.Low;
            this.homeworkNotebookUserControl1.Size = new System.Drawing.Size(1444, 600);
            this.homeworkNotebookUserControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.homeworkNotebookUserControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1454, 600);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "CS 3212";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // homeworkNotebookUserControl2
            // 
            this.homeworkNotebookUserControl2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.homeworkNotebookUserControl2.Location = new System.Drawing.Point(0, 3);
            this.homeworkNotebookUserControl2.Name = "homeworkNotebookUserControl2";
            this.homeworkNotebookUserControl2.Priority = HomeworkNotebookControl.HomeworkNotebookUserControl.PriorityLevel.Low;
            this.homeworkNotebookUserControl2.Size = new System.Drawing.Size(1444, 594);
            this.homeworkNotebookUserControl2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.homeworkNotebookUserControl3);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1454, 600);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "CS 4985";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // homeworkNotebookUserControl3
            // 
            this.homeworkNotebookUserControl3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.homeworkNotebookUserControl3.Location = new System.Drawing.Point(0, 3);
            this.homeworkNotebookUserControl3.Name = "homeworkNotebookUserControl3";
            this.homeworkNotebookUserControl3.Priority = HomeworkNotebookControl.HomeworkNotebookUserControl.PriorityLevel.Low;
            this.homeworkNotebookUserControl3.Size = new System.Drawing.Size(1444, 594);
            this.homeworkNotebookUserControl3.TabIndex = 1;
            // 
            // coursesLabel
            // 
            this.coursesLabel.AutoSize = true;
            this.coursesLabel.Location = new System.Drawing.Point(20, 27);
            this.coursesLabel.Name = "coursesLabel";
            this.coursesLabel.Size = new System.Drawing.Size(92, 25);
            this.coursesLabel.TabIndex = 1;
            this.coursesLabel.Text = "Courses";
            // 
            // tasksToCompleteLabel
            // 
            this.tasksToCompleteLabel.AutoSize = true;
            this.tasksToCompleteLabel.Location = new System.Drawing.Point(30, 731);
            this.tasksToCompleteLabel.Name = "tasksToCompleteLabel";
            this.tasksToCompleteLabel.Size = new System.Drawing.Size(204, 25);
            this.tasksToCompleteLabel.TabIndex = 2;
            this.tasksToCompleteLabel.Text = "Tasks To Complete:";
            // 
            // summaryOutputTextBox
            // 
            this.summaryOutputTextBox.Location = new System.Drawing.Point(35, 775);
            this.summaryOutputTextBox.Multiline = true;
            this.summaryOutputTextBox.Name = "summaryOutputTextBox";
            this.summaryOutputTextBox.ReadOnly = true;
            this.summaryOutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.summaryOutputTextBox.Size = new System.Drawing.Size(1439, 270);
            this.summaryOutputTextBox.TabIndex = 3;
            // 
            // HomeworkNotebook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1506, 1073);
            this.Controls.Add(this.summaryOutputTextBox);
            this.Controls.Add(this.tasksToCompleteLabel);
            this.Controls.Add(this.coursesLabel);
            this.Controls.Add(this.homeworkNotebookTabControl);
            this.Name = "HomeworkNotebook";
            this.Text = "Homework Notebook by Daniel Burkhart";
            this.homeworkNotebookTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl homeworkNotebookTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label coursesLabel;
        private HomeworkNotebookControl.HomeworkNotebookUserControl homeworkNotebookUserControl1;
        private HomeworkNotebookControl.HomeworkNotebookUserControl homeworkNotebookUserControl2;
        private HomeworkNotebookControl.HomeworkNotebookUserControl homeworkNotebookUserControl3;
        private System.Windows.Forms.Label tasksToCompleteLabel;
        private System.Windows.Forms.TextBox summaryOutputTextBox;
    }
}

